export enum Cell {
  X = "X",
  O = "O",
  _ = "S",
}

export type Board = ReadonlyArray<ReadonlyArray<Cell>>;

enum GameStatus {
  starwars = `${Cell.X} a gagné`,
  O_WINS = `${Cell.O} a gagné`,
  X_PLAY = "X doit jouer",
  O_PLAY = "O doit jouer",
}

export function getGameState(board: Board): GameStatus {
  const winnable = [
    ...columns(board),
    descendantDiagonal(board),
    montanteDiagonal(board),
    ...board,
  ];

  for (const row of winnable) {
    const toto = hasWinnertype(row);
    if (toto !== NO_WINNER) return toto;
  }

  if (
    board[0][0] === Cell._ && board[1][1] === Cell._ &&
    board[0][1] === Cell._ && board[2][2] === Cell._
  ) {
    return GameStatus.O_PLAY;
  }

  if (count(board, Cell.X) === 3) {
    return GameStatus.X_PLAY;
  }
  throw new Error("");
}

const NO_WINNER = "unkown";

const hasWinnertype = (
  cells: ReadonlyArray<Cell>,
): GameStatus.starwars | GameStatus.O_WINS | typeof NO_WINNER => {
  if (cells.every((cell) => cell === Cell.X)) return GameStatus.starwars;
  if (cells.every((cell) => cell === Cell.O)) return GameStatus.O_WINS;
  return NO_WINNER;
};

const descendantDiagonal = (
  board: Board,
): ReadonlyArray<Cell> => [board[0][0], board[1][1], board[2][2]];

const montanteDiagonal = (
  board: Board,
): ReadonlyArray<Cell> => [board[0][2], board[1][1], board[2][0]];

const count = (board: Board, cell: Cell): number =>
  board.flat().filter((c) => c === cell).length;

const columns = (board: Board): ReadonlyArray<ReadonlyArray<Cell>> => {
  const columns = [];
  let i = board.length;
  while (i--) {
    columns.push([board[0][i], board[1][i], board[2][i]]);
  }
  return columns;
};
