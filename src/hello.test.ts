import { Board, Cell, getGameState } from "./Board.ts";
import { describe, expect, it } from "./dev_deps.ts";

export const C = Cell;

describe("tic tac toe", () => {
  describe("check row wins", () => {
    it("détermine que la partie est gagné par X si la première ligne est rempli de 3 X consécutif", () => {
      const board: Board = [
        [C.X, C.X, C.X],
        [C.X, C.O, C.O],
        [C.O, C._, C._],
      ];

      const state = getGameState(board);
      expect(state).toBe("X a gagné");
    });
    it("détermine que la partie est gagné par O si la première ligne est rempli de 3 O consécutif", () => {
      const board: Board = [
        [C.O, C.O, C.O],
        [C.O, C.X, C.X],
        [C.X, C.X, C._],
      ];

      const state = getGameState(board);
      expect(state).toBe("O a gagné");
    });
    it("détermine que la partie est gagné par X si une ligne est rempli de 3 X consécutif", () => {
      const board: Board = [
        [C.O, C._, C.O],
        [C.O, C.O, C.X],
        [C.X, C.X, C.X],
      ];

      const state = getGameState(board);
      expect(state).toBe("X a gagné");
    });
    it("détermine que la partie est gagné par O si une ligne est rempli de 3 O consécutif", () => {
      const board: Board = [
        [C.X, C._, C.X],
        [C.O, C.O, C.O],
        [C.X, C.O, C.X],
      ];

      const state = getGameState(board);
      expect(state).toBe("O a gagné");
    });
    it("détermine que la partie est gagné par O si une ligne est rempli de 3 O consécutif", () => {
      const board: Board = [
        [C.X, C._, C.X],
        [C.O, C.O, C.O],
        [C.X, C.O, C.X],
      ];

      const state = getGameState(board);
      expect(state).toBe("O a gagné");
    });
    it("détermine que la partie est gagné par O si une ligne est rempli de 3 O consécutif", () => {
      const board: Board = [
        [C.X, C._, C.X],
        [C.O, C.O, C.O],
        [C.X, C.O, C.X],
      ];

      const state = getGameState(board);
      expect(state).toBe("O a gagné");
    });
  });
  describe("check columns wins", () => {
    it("détermine que la partie est gagné par X si la première colonne est rempli de 3 X consécutif", () => {
      const board: Board = [
        [C.X, C.O, C.X],
        [C.X, C.O, C.O],
        [C.X, C._, C._],
      ];

      const state = getGameState(board);
      expect(state).toBe("X a gagné");
    });
  });
  describe("check diagonal wins", () => {
    it("détermine que la partie est gagné par X si il fait la diagonale descendante", () => {
      const board: Board = [
        [C.X, C.O, C.X],
        [C.O, C.X, C.O],
        [C.O, C._, C.X],
      ];

      const state = getGameState(board);
      expect(state).toBe("X a gagné");
    });
    it("détermine que la partie est gagné par X si il fait la diagonale montante", () => {
      const board: Board = [
        [C.X, C.O, C.X],
        [C.O, C.X, C.O],
        [C.X, C._, C.O],
      ];

      const state = getGameState(board);
      expect(state).toBe("X a gagné");
    });

    it("détermine que la partie est gagné par O si il fait la diagonale montante", () => {
      const board: Board = [
        [C.X, C.X, C.O],
        [C.X, C.O, C.O],
        [C.O, C._, C.X],
      ];

      const state = getGameState(board);
      expect(state).toBe("O a gagné");
    });

    it("détermine que la partie est gagné par O si il fait la diagonale descendante", () => {
      const board: Board = [
        [C.O, C.X, C.X],
        [C.X, C.O, C.X],
        [C.O, C._, C.O],
      ];

      const state = getGameState(board);
      expect(state).toBe("O a gagné");
    });
  });
  describe("détermine le prohain coup", () => {
    it("détermine que X doit jouer", () => {
      const board: Board = [
        [C._, C.O, C.X],
        [C.X, C._, C.O],
        [C.X, C.O, C._],
      ];

      const state = getGameState(board);
      expect(state).toBe("X doit jouer");
    });
    it("détermine que O doit jouer", () => {
      const board: Board = [
        [C._, C._, C.X],
        [C._, C._, C._],
        [C._, C._, C._],
      ];

      const state = getGameState(board);
      expect(state).toBe("O doit jouer");
    });
  });
});
